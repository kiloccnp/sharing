import csv
import boto3

# AWS credentials and region
aws_access_key = 'YOUR_ACCESS_KEY'
aws_secret_key = 'YOUR_SECRET_KEY'
aws_region = 'YOUR_AWS_REGION'

# Initialize the EC2 and RDS clients
ec2_client = boto3.client('ec2', aws_access_key_id=aws_access_key, aws_secret_access_key=aws_secret_key, region_name=aws_region)
rds_client = boto3.client('rds', aws_access_key_id=aws_access_key, aws_secret_access_key=aws_secret_key, region_name=aws_region)

# Get EC2 instances
ec2_instances = ec2_client.describe_instances()

# Get RDS instances
rds_instances = rds_client.describe_db_instances()

# Output CSV file path
output_csv_path = 'aws_instances_status.csv'

# Open CSV file in write mode
with open(output_csv_path, 'w', newline='') as csv_file:
    # Create CSV writer
    csv_writer = csv.writer(csv_file)

    # Write header
    csv_writer.writerow(['Instance Type', 'Instance ID', 'Instance Type', 'Status', 'EBS Volume ID', 'Volume Type', 'Volume Size (GB)'])

    # Write EC2 instance information to the CSV file
    for reservation in ec2_instances['Reservations']:
        for instance in reservation['Instances']:
            instance_id = instance['InstanceId']
            instance_type = instance['InstanceType']
            state = instance['State']['Name']

            # Check if there are attached EBS volumes
            if 'BlockDeviceMappings' in instance:
                for block_device in instance['BlockDeviceMappings']:
                    ebs_volume_id = block_device['Ebs']['VolumeId']
                    volume_type = block_device['Ebs']['VolumeType']
                    volume_size = block_device['Ebs']['VolumeSize']
                    csv_writer.writerow(['EC2', instance_id, instance_type, state, ebs_volume_id, volume_type, volume_size])
            else:
                # If no EBS volumes are attached
                csv_writer.writerow(['EC2', instance_id, instance_type, state, 'N/A', 'N/A', 'N/A'])

    # Write RDS instance information to the CSV file
    for rds_instance in rds_instances['DBInstances']:
        db_instance_id = rds_instance['DBInstanceIdentifier']
        state = rds_instance['DBInstanceStatus']
        csv_writer.writerow(['RDS', db_instance_id, 'N/A', state, 'N/A', 'N/A', 'N/A'])

print(f"Instances status with EBS volume, disk type, and volume size information written to {output_csv_path}")
