import pandas as pd

def get_max_line(excel_file, sheet_name, column_name):
    # Read the Excel file
    df = pd.read_excel(excel_file, sheet_name=sheet_name)

    # Find the row with the maximum value in the specified column
    max_row = df.loc[df[column_name].idxmax()]

    return max_row

if __name__ == "__main__":
    # Set the file path, sheet name, and column name
    excel_file_path = "your_excel_file.xlsx"
    sheet_name = "Sheet1"
    column_name = "YourColumnName"

    # Get the row with the maximum value in the specified column
    max_row = get_max_line(excel_file_path, sheet_name, column_name)

    # Display the result
    print("Row with the maximum value:")
    print(max_row)


import pandas as pd
import matplotlib.pyplot as plt

def create_chart(excel_file, sheet_name, column_name, chart_title, x_label, y_label, output_file):
    # Read the Excel file
    df = pd.read_excel(excel_file, sheet_name=sheet_name)

    # Extract the specified column
    column_data = df[column_name]

    # Plot the chart
    plt.plot(column_data)
    plt.title(chart_title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)

    # Save the chart to an image file
    plt.savefig(output_file)

    # Display the chart
    plt.show()

if __name__ == "__main__":
    # Set the file path, sheet name, column name, and chart details
    excel_file_path = "your_excel_file.xlsx"
    sheet_name = "Sheet1"
    column_name = "YourColumnName"
    chart_title = "Chart Title"
    x_label = "X Axis Label"
    y_label = "Y Axis Label"
    output_file_path = "output_chart.png"

    # Create and display the chart
    create_chart(excel_file_path, sheet_name, column_name, chart_title, x_label, y_label, output_file_path)
