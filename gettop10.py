import pandas as pd
import matplotlib.pyplot as plt
import random
def get_top_values(file_path, sheet_name, num_values):
    # Đọc file Excel vào DataFrame
    df = pd.read_excel(file_path, sheet_name=sheet_name)
    #print(df)
    # Lấy 10 giá trị lớn nhất trong cột A
    top_values = df.nlargest(num_values, "huhu")


    return top_values

def plot_bar_chart(df):
    # Tạo figure với kích thước lớn hơn
    fig = plt.figure(figsize=(10, 6))

    # Tạo subplot với kích thước khung nhỏ hơn
    ax = fig.add_subplot(1, 1, 1)
    # Tạo danh sách các màu sắc ngẫu nhiên
    colors = ['#' + ''.join(random.choices('0123456789ABCDEF', k=6)) for _ in range(len(df))]

    # Vẽ biểu đồ với giá trị của cột B tương ứng
    bars = ax.bar(df['Rent'], df['huhu'], color=colors)
    ax.set_xlabel('Rent', fontsize=16)
    ax.set_ylabel('huhu', fontsize=16)

    # Hiển thị dữ liệu trên đỉnh mỗi cột
    for bar in bars:
        height = bar.get_height()
        ax.text(bar.get_x() + bar.get_width() / 2, height, str(height), ha='center', va='bottom')
    # Xoay và thu nhỏ chữ trục x nếu dài hơn 15 ký tự
    if len(max(df['Rent'], key=len)) > 2:
        ax.set_xticklabels(df['Rent'], rotation=90)
        plt.subplots_adjust(bottom=0.25)
    plt.title("kilo")
    plt.show()

def plot_pie_chart(df):
    # Tạo figure với kích thước lớn hơn
    plt.figure(figsize=(10, 6))

    # Tạo danh sách các màu sắc ngẫu nhiên
    colors = ['#' + ''.join(random.choices('0123456789ABCDEF', k=6)) for _ in range(len(df))]

    # Vẽ biểu đồ với giá trị của cột B tương ứng
    plt.pie(df["huhu"],
            labels=df["Rent"],  # Nhãn của các nhóm
            colors=colors,  # Màu sắc của các nhóm
            autopct='%1.1f%%',  # Format hiển thị giá trị %
            shadow=False)
    plt.title("Average of Petal Length", fontsize=18)
    plt.show()

def plot_scatter_chart(df):
    # Tạo danh sách các màu sắc ngẫu nhiên
    colors = ['#' + ''.join(random.choices('0123456789ABCDEF', k=6)) for _ in range(len(df))]

    # Vẽ biểu đồ với giá trị của cột B tương ứng
    plt.figure(figsize=(16, 8))
    plt.scatter(df["Rent"],df["huhu"], color=colors, )
    plt.xlabel('Petal.Length', fontsize=16)
    plt.ylabel('Petal.Width', fontsize=16)
    plt.title("Average of Petal Length", fontsize=18)
    plt.show()

# Đường dẫn tới file Excel
file_path = 'Expenses01.xlsx'

# Tên sheet trong file Excel
sheet_name = 'Data01'

# Số lượng giá trị lớn nhất muốn lấy
num_values = 10

# Lấy 10 giá trị lớn nhất trong cột A
top_values = get_top_values(file_path, sheet_name, num_values)
print(top_values)
# Vẽ biểu đồ với giá trị của cột B tương ứng
#plot_bar_chart(top_values)
# plot_pie_chart(top_values)
# plot_scatter_chart(top_values)

# doc https://phamdinhkhanh.github.io/deepai-book/ch_appendix/appendix_matplotlib.html
top_values = top_values.drop(["Rent"], axis=1)
print(top_values)

# top_values['Sum'] = top_values.sum(axis=1)
# print(top_values)
#
#
# top_values.loc["Total"] = top_values.sum()
# print(top_values)


### Tính tổng hàng và cột
# df = pd.DataFrame({'a': [10,20],'b':[100,200],'c': ['a','b']})
#
# df.loc['Column_Total']= df.sum(numeric_only=True, axis=0)
# df.loc[:,'Row_Total'] = df.sum(numeric_only=True, axis=1)
# print(df)

# Lọc dữ liệu trùng lặp trong một cột
duplicates = top_values.duplicated(subset='haha', keep=False)
print(duplicates)
# Tính số lượng các giá trị bị trùng lặp
#duplicate_counts = duplicates.value_counts()
duplicate_counts = top_values[duplicates]['haha'].value_counts()
print(duplicate_counts)

# # Vẽ biểu đồ pie tính % từng giá trị bị trùng lặp
plt.pie(duplicate_counts, labels=duplicate_counts.index, autopct='%1.1f%%')
plt.title('Percentage of Duplicate Values')
plt.show()