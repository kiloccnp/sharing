import boto3

def assume_role(role_arn):
    sts_client = boto3.client('sts')
    assumed_role = sts_client.assume_role(
        RoleArn=role_arn,
        RoleSessionName='AssumedRoleSession'
    )
    return boto3.Session(
        aws_access_key_id=assumed_role['Credentials']['AccessKeyId'],
        aws_secret_access_key=assumed_role['Credentials']['SecretAccessKey'],
        aws_session_token=assumed_role['Credentials']['SessionToken']
    )

# Assume role and create an EC2 client
role_session = assume_role('your_role_arn')
ec2_client = role_session.client('ec2', region_name='your_region')

# Assume role and create an S3 client
s3_client = role_session.client('s3', region_name='your_region')

# Now you can use ec2_client and s3_client without re-assuming the role
