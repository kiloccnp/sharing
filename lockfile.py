import os
import sys
import time
from multiprocessing import Lock

lock_file_path = "/tmp/my_script.lock"

def acquire_lock(lock):
    try:
        with lock:
            # Perform any additional checks if needed
            if os.path.exists(lock_file_path):
                print("Another instance is already running. Exiting.")
                sys.exit(1)
            else:
                with open(lock_file_path, 'w') as lock_file:
                    lock_file.write(str(os.getpid()))
    except IOError:
        print("Error acquiring lock. Exiting.")
        sys.exit(1)

def release_lock(lock):
    try:
        with lock:
            os.remove(lock_file_path)
    except IOError:
        print("Error releasing lock.")

def main():
    lock = Lock()

    acquire_lock(lock)

    # Your main script logic goes here
    for _ in range(50):
        print("Working...")
        time.sleep(1)

    release_lock(lock)

if __name__ == "__main__":
    main()
