import csv
import boto3

# AWS credentials and region
aws_access_key = 'YOUR_ACCESS_KEY'
aws_secret_key = 'YOUR_SECRET_KEY'
aws_region = 'YOUR_AWS_REGION'

# Initialize the EC2 and RDS clients
ec2_client = boto3.client('ec2', aws_access_key_id=aws_access_key, aws_secret_access_key=aws_secret_key, region_name=aws_region)
rds_client = boto3.client('rds', aws_access_key_id=aws_access_key, aws_secret_access_key=aws_secret_key, region_name=aws_region)

# Get EC2 instances
ec2_instances = ec2_client.describe_instances()

# Get RDS instances
rds_instances = rds_client.describe_db_instances()

# Output CSV file path
output_csv_path = 'aws_instances_status.csv'

# Open CSV file in write mode
with open(output_csv_path, 'w', newline='') as csv_file:
    # Create CSV writer
    csv_writer = csv.writer(csv_file)

    # Write header
    csv_writer.writerow(['Instance Type', 'Instance ID', 'Instance Type', 'Status'])

    # Write EC2 instance information to the CSV file
    for reservation in ec2_instances['Reservations']:
        for instance in reservation['Instances']:
            instance_id = instance['InstanceId']
            instance_type = instance['InstanceType']
            state = instance['State']['Name']
            csv_writer.writerow(['EC2', instance_id, instance_type, state])

    # Write RDS instance information to the CSV file
    for rds_instance in rds_instances['DBInstances']:
        db_instance_id = rds_instance['DBInstanceIdentifier']
        state = rds_instance['DBInstanceStatus']
        csv_writer.writerow(['RDS', db_instance_id, 'N/A', state])

print(f"Instances status with instance type written to {output_csv_path}")
