import boto3

def list_instances_using_assume_role(role_arn):
    # Assume Role
    sts_client = boto3.client('sts')
    assumed_role = sts_client.assume_role(
        RoleArn=role_arn,
        RoleSessionName='AssumedRoleSession'
    )
    
    # Create an EC2 client using the assumed role credentials
    ec2_client = boto3.client(
        'ec2',
        aws_access_key_id=assumed_role['Credentials']['AccessKeyId'],
        aws_secret_access_key=assumed_role['Credentials']['SecretAccessKey'],
        aws_session_token=assumed_role['Credentials']['SessionToken']
    )

    # List EC2 instances
    instances = ec2_client.describe_instances()
    
    for reservation in instances['Reservations']:
        for instance in reservation['Instances']:
            print(f"Instance ID: {instance['InstanceId']}, State: {instance['State']['Name']}")

# Replace 'your_role_arn' with the ARN of the role you want to assume
list_instances_using_assume_role('your_role_arn')
