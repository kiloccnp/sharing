import boto3

class S3BucketMetrics:
    def __init__(self):
        self.s3_client = boto3.client('s3')
        self.cloudwatch_client = boto3.client('cloudwatch')
        self.metric_name = 'BucketSizeBytes'
        self.namespace = 'AWS/S3'
        self.period = 86400  # 1 day
        self.start_time = '2023-01-01T00:00:00Z'
        self.end_time = '2023-12-31T23:59:59Z'

    def list_s3_buckets(self):
        response = self.s3_client.list_buckets()
        return [bucket['Name'] for bucket in response['Buckets']]

    def get_bucket_size(self, bucket_name):
        metric_data_query = [
            {
                'Id': 'm1',
                'MetricStat': {
                    'Metric': {
                        'Namespace': self.namespace,
                        'MetricName': self.metric_name,
                        'Dimensions': [
                            {
                                'Name': 'BucketName',
                                'Value': bucket_name
                            },
                        ]
                    },
                    'Period': self.period,
                    'Stat': 'Maximum',
                },
                'ReturnData': True,
            },
        ]

        response = self.cloudwatch_client.get_metric_data(
            MetricDataQueries=metric_data_query,
            StartTime=self.start_time,
            EndTime=self.end_time
        )

        if 'Values' in response['MetricDataResults'][0]:
            size_bytes = response['MetricDataResults'][0]['Values'][0]
            return size_bytes
        else:
            return None

    def display_bucket_metrics(self):
        buckets = self.list_s3_buckets()

        for bucket in buckets:
            size_bytes = self.get_bucket_size(bucket)
            if size_bytes is not None:
                print(f"Bucket: {bucket}, Size: {size_bytes} bytes")
            else:
                print(f"Unable to retrieve size for bucket: {bucket}")

# Sử dụng class
s3_metrics = S3BucketMetrics()
s3_metrics.display_bucket_metrics()
