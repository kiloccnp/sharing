import pandas as pd
from datetime import datetime
def read_excel_data(file_path, sheet_name):
    """
    Đọc dữ liệu từ file Excel vào DataFrame
    """
    df = pd.read_excel(file_path, sheet_name=sheet_name)
    return df

def calculate_column_sums(df):
    """
    Tính tổng của từng cột
    """
    column_sums = df.sum()
    return column_sums

def calculate_row_sums(df):
    """
    Tính tổng của từng hàng ngang
    """
    #row_sums = df.sum(axis=1)
    row_sums = df.sum()
    return row_sums

def add_totals_to_dataframe(df, column_sums, row_sums):
    """
    Thêm tổng của từng cột và hàng ngang vào DataFrame
    """
    #df['Total Column'] = column_sums
    df.loc[df.index[-1], 'Total Row'] = row_sums
    return df

def save_dataframe_to_excel(df, output_file_path):
    """
    Lưu DataFrame vào file Excel
    """
    df.to_excel(output_file_path, index=False)

# Sử dụng các hàm đã viết
file_path = 'Expenses01.xlsx'
sheet_name = 'Data01'

df = read_excel_data(file_path, sheet_name)
column_sums = calculate_column_sums(df)
row_sums = calculate_row_sums(df)
#df = add_totals_to_dataframe(df, column_sums, row_sums)
#
# output_file_path = 'path_to_output_file.xlsx'
# save_dataframe_to_excel(df, output_file_path)

# accomodate windows and unix path
# timestamp = datetime.now().isoformat(timespec="minutes").replace(":", "-")
# print(timestamp)