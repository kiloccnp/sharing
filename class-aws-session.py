import boto3

class AssumedRoleSession:
    def __init__(self, role_arn, session_name='AssumedRoleSession'):
        self.role_arn = role_arn
        self.session_name = session_name
        self._session = self._assume_role()

    def _assume_role(self):
        sts_client = boto3.client('sts')
        assumed_role = sts_client.assume_role(
            RoleArn=self.role_arn,
            RoleSessionName=self.session_name
        )
        return boto3.Session(
            aws_access_key_id=assumed_role['Credentials']['AccessKeyId'],
            aws_secret_access_key=assumed_role['Credentials']['SecretAccessKey'],
            aws_session_token=assumed_role['Credentials']['SessionToken']
        )

    def get_ec2_client(self, region_name='your_region'):
        return self._session.client('ec2', region_name=region_name)

    def get_s3_client(self, region_name='your_region'):
        return self._session.client('s3', region_name=region_name)

# Usage
role_session = AssumedRoleSession('your_role_arn')

# Create an EC2 client
ec2_client = role_session.get_ec2_client()

# Create an S3 client
s3_client = role_session.get_s3_client()

# Now you can use ec2_client and s3_client without re-assuming the role
